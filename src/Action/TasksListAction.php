<?php

namespace App\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Factory\LoggerFactory;
use Psr\Log\LoggerInterface;
//use DebugBar\StandardDebugBar; // Отладочная панель нужна только в браузере
use App\Service\TaskListService;

final class TasksListAction
{
    private $logger;
    private $loggerGrafana;
    private $service;

    public function __construct(LoggerFactory $logger, LoggerFactory $loggerGrafana, TaskListService $service)
    {
        $this->logger = $logger
            ->addFileHandler('taskslist.log')
            ->createLogger();
        $this->loggerGrafana = $loggerGrafana
            ->addGrafanaHandler('grafana.log')
            ->createLogger();
        $this->service = $service;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        // Log success
        $this->logger->info('Готов список нарядов');
        $this->loggerGrafana->info('И сюда запиши');

        

        /**
         * Отладочная панель нужна только в браузере
         */
        //$debugbar = new StandardDebugBar();
        //$debugbarRenderer = $debugbar->getJavascriptRenderer('phpdebugbar');
        //$debugbar["messages"]->addMessage("hello world!");

        /* Этот response пока что просто закомментирую
        $response->getBody()->write('<html>
        <head>'
            . $debugbarRenderer->renderHead() .
        '</head>
        <body>
            Это список нарядов ' .
            $debugbarRenderer->render() .
        '</body>
    </html>');

        return $response;*/

        $result = $this->service->getTasks();

        $response->getBody()->write($result);

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}
