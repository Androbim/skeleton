<?php

namespace App\Repository;

class TaskShowRepository
{
    
    private $testFile;



        
    /**
     * __construct
     *
     * @return void
     * 
     * В дальнейшем сюда будет передана зависимость, наверное, внедрение через DI-контейнер
     * А сейчас просто json-строка
     */
    public function __construct()
    {
        // Исключительно для теста
        $testTaskShowResponse = '
        {
            "status": "SUCCESS",
            "status_msg": "Данные наряда обновлены",
            "status_code": "task",
            "sessid": "8b21cadf5861f184584f359dd95ec034",
            "data": {
                "task_source_system": "ASRZ",
                "status_group": "active",
                "assignment_start": "20201115T100000+0300",
                "assignment_finish": "20201115T112600+0300",
                "task_create_date": "20201112T183103+0300",
                "phone": "4991437197",
                "mobile_phone": "79854167785",
                "contact": "ПРЕОБРАЖЕНСКАЯ НАТАЛЬЯ ВЛАДИМИРОВНА",
                "task_status": "В работе",
                "task_status_allow_actions": true,
                "task_type": "Первичное переключение абонентов на PON",
                "not_ready": false,
                "online": false,
                "wfm_key": "34961241",
                "appointment_start": "20201115T100000+0300",
                "appointment_finish": "20201115T120000+0300",
                "desired_time_range": "15.11.2020 | 10:00 - 12:00",
                "address": "Москва, Университетский просп., д.23, корп.2, кв.86, подъезд 1, этаж 1",
                "latitude": 55.714817,
                "longitude": 37.518721,
                "uspd": "28123066",
                "task_comment": "2021-01-16 13:42:09 Статус наряда = В работе",
                "customer_name": "ПРЕОБРАЖЕНСКАЯ НАТАЛЬЯ ВЛАДИМИРОВНА",
                "agreement": "681074018-1/2014",
                "cc_task_comment": "Можно переотправить эту заявку в УСПД! Старая канальная заявка отключена! KDA эт 6 пд 3 код 86в 9639222142",
                "special_attributes": "эт 6 пд 3 код 86в 9639222142; Тип ОРК: без коннекторов",
                "ork_type": "без коннекторов",
                "ork_port": "null",
                "fiber_color": null,
                "first_diagnostics": null,
                "current_diagnostics": null,
                "problem_comment": null,
                "line_data": "931-0723-1АТШ—0723-1/1(143)—3—003/1003—143-13—093",
                "junction_box_address": "РФ, г. Москва, Университетский просп., 23,к.2, 86",
                "active_equipment_allow_changes": true,
                "short_address": "Москва, Университетский просп., д.23, корп.2, кв.86",
                "task_type_allow_edo": true,
                "task_type_allow_edo_note": "",
                "task_status_equipment_sn_recovery": true,
                "task_status_allow_edo": false,
                "task_status_allow_edo_note": "Для текущего статуса наряда формирование ЭДО невозможно",
                "task_status_allow_repair": true,
                "wfm_id": "2828326750825002192",
                "session_allow_reset": true,
                "customer": {
                    "personal_account_number": "681074018",
                    "delivery_type": [
                        "postal"
                    ],
                    "delivery_email": null,
                    "available_delivery_types": [
                        "account",
                        "email"
                    ],
                    "available_delivery": [
                        {
                            "delivery_code": "1",
                            "delivery_name": "E-mail"
                        },
                        {
                            "delivery_code": "3",
                            "delivery_name": "Доставка в ЛК"
                        }
                    ],
                    "delivery_code": "0",
                    "delivery_name": "Почта"
                },
                "task_statuses_note": "Для получения возможности закрыть наряд, убедитесь, что метод доставки email и адрес электронной почты заполнен.",
                "task_statuses_allowed": [],
                "customer_b2b": {
                    "project_b2b": "",
                    "investproject_b2b": ""
                },
                "kross_uspd": "PON\r\nУниветситетский пр-т. д. 23к.2",
                "services_status_allow_changes": false,
                "service_provider": "5",
                "is_paid_repair": false,
                "comment_move": "",
                "task_type_needs_diagnostics": false,
                "operator_id": "12",
                "courier_address": "Москва, Университетский, дом 23, корпус 2, кв. 86",
                "courier_address_comment": "",
                "new_services": [
                    {
                        "service_name": "Интернет",
                        "service_items": [
                            {
                                "is_first_call": false,
                                "service_uspd": "28122677",
                                "basic_attributes": [
                                    {
                                        "code": "service_task_shifr",
                                        "value": null,
                                        "label": "Шифр закрытия"
                                    },
                                    {
                                        "code": "service_task_status",
                                        "value": null,
                                        "label": "Статус"
                                    },
                                    {
                                        "code": "service_tariff_name",
                                        "value": "PON-204800:AFH_2020",
                                        "label": "Тарифный план"
                                    },
                                    {
                                        "code": "service_asrz_number",
                                        "value": "47735694",
                                        "label": "№ АСРЗ"
                                    },
                                    {
                                        "code": "service_asrz_stage",
                                        "value": "Ожидание авторизации",
                                        "label": "Этап заявки АСРЗ"
                                    }
                                ],
                                "statuses_allowed": []
                            }
                        ]
                    },
                    {
                        "service_name": "Телефон",
                        "service_items": [
                            {
                                "is_first_call": true,
                                "service_uspd": "28122678",
                                "basic_attributes": [
                                    {
                                        "code": "service_task_shifr",
                                        "value": null,
                                        "label": "Шифр закрытия"
                                    },
                                    {
                                        "code": "service_task_status",
                                        "value": null,
                                        "label": "Статус"
                                    },
                                    {
                                        "code": "service_tariff_name",
                                        "value": "1",
                                        "label": "Тарифный план"
                                    },
                                    {
                                        "code": "service_asrz_number",
                                        "value": "47735695",
                                        "label": "№ АСРЗ"
                                    },
                                    {
                                        "code": "service_asrz_stage",
                                        "value": "Включение в УСПД",
                                        "label": "Этап заявки АСРЗ"
                                    }
                                ],
                                "statuses_allowed": []
                            }
                        ]
                    }
                ],
                "services": {
                    "onetime": null,
                    "internet": [
                        {
                            "service_type_name": "Интернет",
                            "service_task_shifr": "",
                            "service_task_status": "",
                            "service_tariff_name": "Всё для дома 2020 - 200 Мбит/с",
                            "service_asrz_number": "47735694",
                            "service_asrz_stage": "Ожидание авторизации"
                        }
                    ],
                    "phone": [
                        {
                            "service_type_name": "Телефон",
                            "service_task_shifr": "",
                            "service_task_status": "",
                            "service_tariff_name": "Повременная система оплаты",
                            "service_asrz_number": "47735695",
                            "service_asrz_stage": "Включение в УСПД"
                        }
                    ],
                    "mvno": null,
                    "tv": null
                },
                "services_onetime": null,
                "new_equipment": [
                    {
                        "equipment_type_name": "ONT",
                        "equipment_type": "ONT",
                        "equipment_items": [
                            {
                                "basic_attributes": [
                                    {
                                        "code": "equipment_task_shifr",
                                        "value": null,
                                        "label": "Шифр закрытия"
                                    },
                                    {
                                        "code": "equipment_task_status",
                                        "value": null,
                                        "label": "Статус"
                                    },
                                    {
                                        "code": "equipment_tariff_name",
                                        "value": null,
                                        "label": "Тарифный план"
                                    },
                                    {
                                        "code": "equipment_holder_type",
                                        "value": "Пользование",
                                        "label": "Владелец оборудования"
                                    },
                                    {
                                        "code": "equipment_model_name",
                                        "value": null,
                                        "label": "Модель"
                                    },
                                    {
                                        "code": "equipment_serial_number",
                                        "value": null,
                                        "label": "Серийный номер"
                                    },
                                    {
                                        "code": "equipment_asrz_number",
                                        "value": "47735693",
                                        "label": "№ АСРЗ"
                                    },
                                    {
                                        "code": "equipment_state_name",
                                        "value": null,
                                        "label": "Состояние"
                                    }
                                ]
                            }
                        ]
                    }
                ],
                "equipment": [
                    {
                        "equipment_type_name": "ONT",
                        "equipment_task_shifr": "",
                        "equipment_task_status": "",
                        "equipment_asrz_number": "47735693",
                        "equipment_tariff_name": "",
                        "equipment_type": "ONT",
                        "equipment_holder_type": "Пользование",
                        "equipment_model_name": "",
                        "equipment_serial_number": ""
                    }
                ],
                "repair": null
            }
        }
        ';
        
        $this->testFile = $testTaskShowResponse;
    }
    
    
    public function getTask() : string
    {
        return $this->testFile;
    }
}