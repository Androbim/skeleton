<?php

namespace App\Repository;

class TaskListRepository
{
    
    private $testFile;



        
    /**
     * __construct
     *
     * @return void
     * 
     * В дальнейшем сюда будет передана зависимость, наверное, внедрение через DI-контейнер
     * А сейчас просто json-строка
     */
    public function __construct()
    {
        // Исключительно для теста
        $testTaskListResponse = '
        {
            "status": "SUCCESS",
            "status_msg": "Список нарядов обновлен",
            "status_code": "tasks_list",
            "sessid": "24b2363b3baab86a79b28ac95489aa6a",
            "data": [
                {
                    "status_group": "active",
                    "assignment_start": "20201115T100000+0300",
                    "assignment_finish": "20201115T112600+0300",
                    "phone": "4991437197",
                    "mobile_phone": "+79854167785",
                    "contact": "ПРЕОБРАЖЕНСКАЯ НАТАЛЬЯ ВЛАДИМИРОВНА",
                    "task_status": "В работе",
                    "task_type": "Первичное переключение абонентов на PON",
                    "task_type_needs_diagnostics": false,
                    "task_type_category": "First",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201112T000000+0300",
                    "wfm_key": "34961241",
                    "wfm_id": "2828326750825002192",
                    "desired_time_range": "15.11.2020 | 10:00 - 12:00",
                    "address": "Москва, Университетский просп., д.23, корп.2, кв.86, подъезд 1, этаж 1",
                    "short_address": "Москва, Университетский просп., д.23, корп.2, кв.86",
                    "latitude": 55.714817,
                    "longitude": 37.518721,
                    "service_provider": "5",
                    "courier_address": "Москва, Университетский, дом 23, корпус 2, кв. 86",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_note": "Для получения возможности закрыть наряд, убедитесь, что метод доставки email и адрес электронной почты заполнен.",
                    "task_statuses_allowed": []
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201115T112800+0300",
                    "assignment_finish": "20201115T120800+0300",
                    "phone": "4991321043",
                    "mobile_phone": "+79057184278",
                    "contact": "Сеничонко Александр Владимирович",
                    "task_status": "Выполнен",
                    "task_type": "Обслуживание абонентов PON",
                    "task_type_needs_diagnostics": true,
                    "task_type_category": "Support",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201113T000000+0300",
                    "wfm_key": "35386837",
                    "wfm_id": "2828664761699010784",
                    "desired_time_range": "15.11.2020 | 14:00 - 16:00",
                    "address": "Москва, Вавилова ул., д.54, корп.1, кв.54, подъезд 1, этаж 1",
                    "short_address": "Москва, Вавилова ул., д.54, корп.1, кв.54",
                    "latitude": 55.688728,
                    "longitude": 37.557238,
                    "service_provider": "5",
                    "courier_address": "Москва, Вавилова, дом 54, корпус 1, кв. 54",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_allowed": []
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201115T120000+0300",
                    "assignment_finish": "20201115T124200+0300",
                    "phone": "4991476116",
                    "mobile_phone": "+79031309264",
                    "contact": "ФЕНИН ВАСИЛИЙ ВАЛЕРЬЕВИЧ",
                    "task_status": "В работе",
                    "task_type": "Подключение услуг на PON",
                    "task_type_needs_diagnostics": false,
                    "task_type_category": "First",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201103T000000+0300",
                    "wfm_key": "986449593",
                    "wfm_id": "2825036589437452988",
                    "desired_time_range": "15.11.2020 | 12:00 - 14:00",
                    "address": "Москва, Мосфильмовская ул., д.13, кв.19, подъезд 1, этаж 1",
                    "short_address": "Москва, Мосфильмовская ул., д.13, кв.19",
                    "latitude": 55.717642,
                    "longitude": 37.52048,
                    "courier_address": "Москва, Мосфильмовская, дом 13, кв. 19",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_note": "Для получения возможности закрыть наряд, убедитесь, что метод доставки email и адрес электронной почты заполнен.",
                    "task_statuses_allowed": []
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201115T130000+0300",
                    "assignment_finish": "20201115T143100+0300",
                    "phone": "0507526172",
                    "mobile_phone": "+79857613927",
                    "contact": "Разумов Сергей Наркисович",
                    "task_status": "Выполнен",
                    "task_type": "Подключение/переключение на PON с ОРК",
                    "task_type_needs_diagnostics": false,
                    "task_type_category": "First",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201113T000000+0300",
                    "wfm_key": "35229050",
                    "wfm_id": "2828555613360334810",
                    "desired_time_range": "15.11.2020 | 13:00 - 15:00",
                    "address": "Москва, Университетский просп., д.6, корп.3, кв.86, подъезд 1, этаж 1",
                    "short_address": "Москва, Университетский просп., д.6, корп.3, кв.86",
                    "latitude": 55.699576,
                    "longitude": 37.552924,
                    "service_provider": "1104",
                    "courier_address": "Москва, Университетский, дом 6, корпус 3, кв. 86",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_note": "Для получения возможности закрыть наряд, убедитесь, что метод доставки email и адрес электронной почты заполнен.",
                    "task_statuses_allowed": []
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201115T160800+0300",
                    "assignment_finish": "20201115T174300+0300",
                    "phone": "0507438637",
                    "mobile_phone": "+79999216846",
                    "contact": "Козлов Алексей Сергеевич",
                    "task_status": "Взят в работу",
                    "task_type": "Подключение/переключение на PON с ОРК",
                    "task_type_needs_diagnostics": false,
                    "task_type_category": "First",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201113T000000+0300",
                    "wfm_key": "35370077",
                    "wfm_id": "2828651698707948760",
                    "desired_time_range": "15.11.2020 | 16:00 - 18:00",
                    "address": "Москва, Ленинский просп., д.60/2, кв.287, подъезд 1, этаж 1",
                    "short_address": "Москва, Ленинский просп., д.60/2, кв.287",
                    "latitude": 55.697328,
                    "longitude": 37.558937,
                    "service_provider": "1104",
                    "courier_address": "Москва, Ленинский, дом 60/2, кв. 287",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_allowed": [
                        "В работе"
                    ]
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201115T180300+0300",
                    "assignment_finish": "20201115T184300+0300",
                    "phone": "4991316847",
                    "mobile_phone": "+79167198835",
                    "contact": "ТЯБИНА АНАСТАСИЯ ВЛАДИМИРОВНА",
                    "task_status": "Взят в работу",
                    "task_type": "Обслуживание абонентов PON",
                    "task_type_needs_diagnostics": true,
                    "task_type_category": "Support",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201114T000000+0300",
                    "wfm_key": "35807506",
                    "wfm_id": "2828961371612080824",
                    "desired_time_range": "15.11.2020 | 17:00 - 19:00",
                    "address": "Москва, Ломоносовский просп., д.23, кв.358, подъезд 1, этаж 1",
                    "short_address": "Москва, Ломоносовский просп., д.23, кв.358",
                    "latitude": 55.690996,
                    "longitude": 37.539052,
                    "service_provider": "5",
                    "courier_address": "Москва, Ломоносовский, дом 23, кв. 358",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_allowed": [
                        "В работе"
                    ]
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201116T080000+0300",
                    "assignment_finish": "20201116T100000+0300",
                    "phone": "0507095236",
                    "mobile_phone": "+79853358574",
                    "contact": "СТРОКОВА ИРИНА МИХАЙЛОВНА",
                    "task_status": "Назначен",
                    "task_type": "Компьютерная помощь - монтаж",
                    "task_type_needs_diagnostics": false,
                    "task_type_category": "First",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201113T000000+0300",
                    "wfm_key": "35340607",
                    "wfm_id": "2828625277307812000",
                    "desired_time_range": "16.11.2020 | 8:00 - 11:00",
                    "address": "Москва, Госпитальный Вал ул., д.5, стр.4, кв.95, комн.2, подъезд 1, этаж 1",
                    "short_address": "Москва, Госпитальный Вал ул., д.5, стр.4, кв.95, комн.2",
                    "latitude": 55.774543,
                    "longitude": 37.709155,
                    "courier_address": "Москва, Госпитальный Вал, дом 5, строение 4, кв. 95",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_allowed": [
                        "Взят в работу"
                    ]
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201116T110000+0300",
                    "assignment_finish": "20201116T130000+0300",
                    "phone": "4997437819",
                    "mobile_phone": "+79671289888",
                    "contact": "АЛЕКСИЧ НАТАЛИЯ АЛЕКСАНДРОВНА",
                    "task_status": "Назначен",
                    "task_type": "Компьютерная помощь - монтаж",
                    "task_type_needs_diagnostics": false,
                    "task_type_category": "First",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201109T000000+0300",
                    "wfm_key": "989532473",
                    "wfm_id": "2827108535210424080",
                    "desired_time_range": "16.11.2020 | 11:00 - 14:00",
                    "address": "Москва, Адмирала Лазарева ул., д.35, кв.173, подъезд 1, этаж 1",
                    "short_address": "Москва, Адмирала Лазарева ул., д.35, кв.173",
                    "latitude": 55.543748,
                    "longitude": 37.524942,
                    "courier_address": "Москва, Адмирала Лазарева, дом 35, кв. 173",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_allowed": [
                        "Взят в работу"
                    ]
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201116T123000+0300",
                    "assignment_finish": "20201116T135600+0300",
                    "phone": "0507525712",
                    "mobile_phone": "+79163300834",
                    "contact": "АРТЮШИНА ЕЛЕНА АЛЕКСЕЕВНА",
                    "task_status": "Назначен",
                    "task_type": "Первичное переключение абонентов на PON",
                    "task_type_needs_diagnostics": false,
                    "task_type_category": "First",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201112T000000+0300",
                    "wfm_key": "34937161",
                    "wfm_id": "2828293130601969032",
                    "desired_time_range": "16.11.2020 | 12:00 - 14:00",
                    "address": "Москва, Покрышкина ул., двл.8, кв.343, подъезд 1, этаж 1",
                    "short_address": "Москва, Покрышкина ул., двл.8, кв.343",
                    "latitude": 55.666476,
                    "longitude": 37.471315,
                    "service_provider": "1104",
                    "courier_address": "Москва, Покрышкина, кв. 343",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_allowed": [
                        "Взят в работу"
                    ]
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201116T130400+0300",
                    "assignment_finish": "20201116T134400+0300",
                    "phone": "4991370959",
                    "mobile_phone": "+79162406040",
                    "contact": "СОРКИНА НАТАЛИЯ НИКОЛАЕВНА",
                    "task_status": "Выполнен",
                    "task_type": "Обслуживание абонентов OTA",
                    "task_type_needs_diagnostics": true,
                    "task_type_category": "Support",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201113T000000+0300",
                    "wfm_key": "35461273",
                    "wfm_id": "2828712805004173093",
                    "desired_time_range": "16.11.2020 | 12:00 - 14:00",
                    "address": "Москва, Ленинский просп., д.60/2, кв.32, подъезд 1, этаж 1",
                    "short_address": "Москва, Ленинский просп., д.60/2, кв.32",
                    "latitude": 55.697328,
                    "longitude": 37.558937,
                    "service_provider": "5",
                    "courier_address": "Москва, Ленинский, дом 60/2, кв. 32",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_note": "Для получения возможности закрыть наряд, убедитесь, что метод доставки email и адрес электронной почты заполнен.",
                    "task_statuses_allowed": []
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201116T170000+0300",
                    "assignment_finish": "20201116T190000+0300",
                    "phone": "4953312814",
                    "mobile_phone": "84953311531",
                    "contact": "КОНОВАЛОВ ДЕНИС ВИКТОРОВИЧ",
                    "task_status": "Назначен",
                    "task_type": "Компьютерная помощь",
                    "task_type_needs_diagnostics": false,
                    "task_type_category": "First",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201113T000000+0300",
                    "wfm_key": "35231100",
                    "wfm_id": "2828555362469466102",
                    "desired_time_range": "16.11.2020 | 17:00 - 20:00",
                    "address": "Москва, Цюрупы ул., д.11, корп.3, кв.57, подъезд 1, этаж 1",
                    "short_address": "Москва, Цюрупы ул., д.11, корп.3, кв.57",
                    "latitude": 55.665742,
                    "longitude": 37.572137,
                    "courier_address": "Москва, Цюрупы, дом 11, корпус 3, кв. 57",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_allowed": [
                        "Взят в работу"
                    ]
                },
                {
                    "status_group": "active",
                    "assignment_start": "20201117T180000+0300",
                    "assignment_finish": "20201117T193100+0300",
                    "phone": "0507527723",
                    "mobile_phone": "89203490911",
                    "contact": "Костерина Марина Викторовна",
                    "task_status": "Назначен",
                    "task_type": "Первичное переключение абонентов на PON",
                    "task_type_needs_diagnostics": false,
                    "task_type_category": "First",
                    "not_ready": false,
                    "online": false,
                    "task_create_date": "20201114T000000+0300",
                    "wfm_key": "35852167",
                    "wfm_id": "2829006103387274173",
                    "desired_time_range": "17.11.2020 | 18:00 - 20:00",
                    "address": "Москва, Мичуринский Проспект, Олимпийская Деревня ул., д.7, кв.173, комн.2",
                    "short_address": "Москва, Мичуринский Проспект, Олимпийская Деревня ул., д.7, кв.173, комн.2",
                    "latitude": 55.675576,
                    "longitude": 37.464592,
                    "service_provider": "1104",
                    "courier_address": "Москва, Мичуринский Проспект, Олимпийская Деревня, дом 7, кв. 173",
                    "appointment_start": null,
                    "appointment_finish": null,
                    "task_statuses_allowed": [
                        "Взят в работу"
                    ]
                }
            ]
        }
        ';
        
        $this->testFile = $testTaskListResponse;
    }
    
    
    public function getTasks() : string
    {
        return $this->testFile;
    }
}