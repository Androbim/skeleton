<?php

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

class AuthMiddleware
{
    /**
     * Пример промежуточного ПО как вызываемый класс.
     * Данное промежуточное ПО дописывает BEFORE в начало ответа
     *
     * @param  Request  $request PSR-7 запрос
     * @param  RequestHandler $handler PSR-15 обработчик запроса
     *
     * @return Response
     */
    public function __invoke(Request $request, RequestHandler $handler): Response
    {
        $response = $handler->handle($request);
        
        /* Код ниже не нужен, если не нужен "видимый" response именно от middleware
        $existingContent = (string) $response->getBody();

        
        $response = new Response();
        $response->getBody()->write('BEFORE' . $existingContent);*/

        // А тут, наверное, должен вызываться сервис, который берет на себя все нюансы аутентификации, а далее - от возврата
        // Нюансы работы с токеном и все такое
        // Возможно и $existingContent  будет то, что нужно

        return $response;
    }
}