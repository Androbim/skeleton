<?php

namespace App\Service;

use App\Repository\TaskShowRepository;

final class TaskShowService
{

    /**
     * TODO: Добавить сюда логирование
     * TODO: Добавить сюда валидацию
     */
    
    private $repository;

    
    public function __construct(TaskShowRepository $repository)
    {
        $this->repository = $repository;
        
    }


    public function getTask(): string
    {
        
        $task = $this->repository->getTask();

        // Logging here: User created successfully
        //$this->logger->info(sprintf('User created successfully: %s', $userId));

        return $task;
    }

}