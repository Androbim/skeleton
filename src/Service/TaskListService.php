<?php

namespace App\Service;

use App\Repository\TaskListRepository;

final class TaskListService
{

    /**
     * TODO: Добавить сюда логирование
     * TODO: Добавить сюда валидацию
     */
    
    private $repository;

    
    public function __construct(TaskListRepository $repository)
    {
        $this->repository = $repository;
        
    }


    public function getTasks(): string
    {
        
        $tasks = $this->repository->getTasks();

        // Logging here: User created successfully
        //$this->logger->info(sprintf('User created successfully: %s', $userId));

        return $tasks;
    }

}