<?php

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use App\Middleware\AuthMiddleware;

return function (App $app) {
    $app->get('/', function (
        ServerRequestInterface $request,
        ResponseInterface $response
    ) {
        $response->getBody()->write('Hello, World!');

        return $response;
    });
    $app->get('/tasks', \App\Action\TasksListAction::class)->add(new AuthMiddleware());
    $app->get('/tasks/{wfm_key}', \App\Action\TaskShowAction::class);
    $app->patch('/tasks/{wfm_key}', \App\Action\TaskPatchAction::class);
    $app->post('/tasks/{wfm_key}', \App\Action\TaskPostAction::class);
};